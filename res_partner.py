#!/usr/bin/env python

from openerp.osv import osv, fields


class res_users(osv.osv):
    _inherit = 'res.partner'
    _columns = {
        #ES - Add VAT field to Partner record
        'vat': fields.char('VAT No.', size=32, help="VAT Number. Used if this contact is subjected to taxes. Used by the some of the legal statements."),

    }


