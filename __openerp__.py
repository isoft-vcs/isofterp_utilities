# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


{
    'name': 'i-SoftERP Utilities',
    'version': '1.0',
    'category': 'Sales',
    'summary': 'i-SoftERP Utilities',
    'description': """
i-SoftERP Standard Utilities
=================================

- Standard customisations
- Add VAT field to partner
- Remove line which reads sent using Odoo in mail communications

    """,
    'author': 'i-Soft Solutions (Pty) Ltd',
    'depends': ['base','account','sale'],
    'data': [
        'mail_followers_view.xml',
        'res_partner_view.xml',

    ],
    'installable': True,
    'application': False,
    'auto_install': False,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
